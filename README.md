# basic math of neural networks - in 4 slides, a spreadsheet and python
this talk is based on the book Grokking Deep Learning by Andrew Trask

## slides
https://docs.google.com/presentation/d/18UkN_Og2D6ZRCAwyKLBUHNPmWr-M6YGAFKiAnUYOaao

## spreadsheet
https://docs.google.com/spreadsheets/d/1AS2fz-0VocX4pf7uyEW8eg7hMEcNZjV6S4Q2ted9QYs