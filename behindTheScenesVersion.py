import numpy as np
import os
from copy import deepcopy
from scipy import signal
import matplotlib.pyplot as plt

outputFilename = "weights"
del_files = ["weights.txt", "output.txt", "error.txt", "errorOverInputs.txt"]
for path in del_files:  
  if os.path.isfile(path):
    os.remove(path)

# set seed to make random stuff reproducible
np.random.seed(1)

# activation
def relu(x):
    return (x > 0) * x # returns x if x > 0
                       # return 0 otherwise

# dericative of activation
def relu2deriv(output):
    return output>0 # returns 1 for input > 0
                    # return 0 otherwise

"""initialize"""
# input layer - 4 different inputs
streetlights = np.array( [[ 1, 0, 1 ],
                          [ 0, 1, 1 ],
                          [ 0, 0, 1 ],
                          [ 1, 1, 1 ] ] )

streetlights = np.array( [[ 1, 0, 1 ]] )

# output layer - 4 different outputs
walk_vs_stop = np.array([[ 1, 1, 0, 0]]).T
walk_vs_stop = np.array([[ 1]]).T

alpha = 1 # learning rate
hidden_size = 4 # number of neurons in hidden layer 

# weights - initialized randomly
weights_0_1 = 2*np.random.random((3,hidden_size)) - 1 # weights between input and hidden layer
weights_1_2 = 2*np.random.random((hidden_size,1)) - 1 # weights between hidden layer and output

# number of itterations
learningLoops = 80 * len(streetlights)

errorOverTime = []
errorOverTimePerNeuron = [[],[],[],[]]

allItterations = []
secondWeightsTime = []
firstWeightsTime0 = []
firstWeightsTime1 = []
firstWeightsTime2 = []

secondWeightsTimePerInput = [[],[],[],[]]
firstWeightsTime0PerInput = [[],[],[],[]]
firstWeightsTime1PerInput = [[],[],[],[]]
firstWeightsTime2PerInput = [[],[],[],[]]

hiddenLayerTime = []
hiddenLayerActivatedTime = []
layer1DeltaTime = []
layer2DeltaTime = []

errorOverTimeMEAN = []

for iteration in range(learningLoops): # loops through set learning loops
  layer_2_error = 0
  for i in range(len(streetlights)): # loops through all inputs
    if iteration == 0:
      hiddenLayerTime.append([])
      hiddenLayerActivatedTime.append([])

    """predict"""
    layer_0 = streetlights[i:i+1]
    layer_1 = relu(np.dot(layer_0,weights_0_1))

    hiddenLayerActivatedCopy = deepcopy(layer_1)
    hiddenLayerTime[i].append(np.dot(layer_0,weights_0_1)[0])
    hiddenLayerActivatedTime[i].append(hiddenLayerActivatedCopy[0])

    layer_2 = np.dot(layer_1,weights_1_2)

    layer_2_error += np.sum((layer_2 - walk_vs_stop[i:i+1]) ** 2)

    """compare"""
    layer_2_delta = (layer_2 - walk_vs_stop[i:i+1])
    layer_1_delta=layer_2_delta.dot(weights_1_2.T)*relu2deriv(layer_1)

    with open(f"{outputFilename}.txt", "a") as f:
          f.write(f"input: {layer_0}\n\n"
          f"layer2 error: {layer_2_error}\n\n"
          f"layer1 - activated: {layer_1}\n\n"
          f"layer2: {layer_2}\n\n"
          f"layer2Delta: {layer_2_delta}\n\n"
          f"layer1Delta: {layer_1_delta}\n\n"
          f"weights0-1:\n{weights_0_1}\n\n"
          f"weights1-2:\n{weights_1_2}\n\n"
          f"---\n")

    """learn"""
    weights_1_2 -= alpha * layer_1.T.dot(layer_2_delta)
    weights_0_1 -= alpha * layer_0.T.dot(layer_1_delta)

    with open(f"{outputFilename}.txt", "a") as f:
          f.write(f"new weights0-1:\n{weights_0_1}\n\n"
          f"new weights1-2:\n{weights_1_2}\n\n"
          f"---------\n\n\n")

    with open(f"error.txt", "a") as f:
      f.write(f"{layer_2_delta}\n")
    errorOverTime.append(layer_2_error)
    errorOverTimePerNeuron[i].append(layer_2_error)

    allItterations.append(iteration)
    
    copyWeights_0_1 = deepcopy(weights_0_1)
    copyWeights_1_2 = deepcopy(weights_1_2)

    firstWeightsTime0.append(copyWeights_0_1[0])
    firstWeightsTime1.append(copyWeights_0_1[1])
    firstWeightsTime2.append(copyWeights_0_1[2])

    firstWeightsTime0PerInput[i].append(copyWeights_0_1[0])
    firstWeightsTime1PerInput[i].append(copyWeights_0_1[1])
    firstWeightsTime2PerInput[i].append(copyWeights_0_1[2])


    secondWeightsTime.append(copyWeights_1_2)
    secondWeightsTimePerInput[i].append(copyWeights_1_2)

  with open(f"errorOverInputs.txt", "a") as f:
    f.write(f"{sum(errorOverTime[-4:]) / 4}\n")
  errorOverTimeMEAN.append(sum(errorOverTime[-len(streetlights):]) / streetlights)

  if(iteration % 10 == 9):
    print("Error:" + str(layer_2_error))

fig = plt.figure()
plt.ylim(-1, 1)
ax = plt.subplot()
#ax.text(0.0, 0.9, 'firstWeightsOverTime')
for i in range(hidden_size):
    plt.plot(allItterations, [item[i] for item in firstWeightsTime0], label=f"input{i}_0")
    plt.plot(allItterations, [item[i] for item in firstWeightsTime1], label=f"input{i}_1")
    plt.plot(allItterations, [item[i] for item in firstWeightsTime2], label=f"input{i}_2")
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15),
          ncol=4, fancybox=True, shadow=True, fontsize='7').get_frame().set_alpha(0.5)
plt.xlabel('iterations', fontsize=10)
plt.ylabel('weights 0->1', fontsize=10)
plt.savefig(f"firstWeightsOverTime.png")
plt.close()

fig = plt.figure()
plt.ylim(-1, 1.5)
ax = plt.subplot()
#ax.text(0.0, 0.9, 'secondWeightsOverTime')
for i in range(hidden_size):
  plt.plot(allItterations, [item[i] for item in secondWeightsTime], label=f"neuron{i}")
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15),
          ncol=4, fancybox=True, shadow=True, fontsize='7').get_frame().set_alpha(0.5)
plt.xlabel('iterations', fontsize=10)
plt.ylabel('weights 1->2', fontsize=10)
plt.savefig(f"secondWeightsOverTime.png")
plt.close()

fig = plt.figure()
plt.ylim(-1, 1)
ax = plt.subplot()
#ax.text(0.0, 0.9, 'hiddenLayer')
for n in range(hidden_size):
  for i in range(len(streetlights)):
    # changed i & n to have better sorting
    plt.plot(np.arange(0, learningLoops).tolist(), [item[n] for item in hiddenLayerTime[i]], 
                label=f"neuron{n}_input{i}", color=f"C{n}")
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15),
          ncol=4, fancybox=True, shadow=True, fontsize='7').get_frame().set_alpha(0.5)
plt.xlabel('iteration', fontsize=10)
plt.ylabel('hidden layer', fontsize=10)
plt.savefig(f"hiddenLayer.png")
plt.close()


fig = plt.figure()
plt.ylim(-1, 1)
ax = plt.subplot()
#ax.text(0.0, 0.9, 'hiddenLayerActivated')
for n in range(hidden_size):
  for i in range(len(streetlights)):
    # changed i & n to have better sorting
    plt.plot(np.arange(0, learningLoops).tolist(), [item[n] for item in hiddenLayerActivatedTime[i]], 
                label=f"neuron{n}_input{i}", color=f"C{n}") #, marker=2)
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15),
          ncol=4, fancybox=True, shadow=True, fontsize='7').get_frame().set_alpha(0.5)
plt.xlabel('iterations', fontsize=10)
plt.ylabel('activated hidden layer', fontsize=10)
plt.savefig(f"hiddenLayerActivated.png")
plt.close()

fig = plt.figure()
ax = plt.subplot()
#ax.text(0.0, 0.9, 'errorOverTime')
plt.plot(allItterations, errorOverTime, label=f"errorOverTime")
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15),
          ncol=4, fancybox=True, shadow=True, fontsize='7').get_frame().set_alpha(0.5)
plt.xlabel('iterations', fontsize=10)
plt.ylabel('error', fontsize=10)
plt.savefig("errorOverTime.png")
plt.close()

"""
fig = plt.figure()
ax = plt.subplot()
ax.text(0.0, 0.9, 'errorOverSecondWeights')
for i in range(hidden_size):
  plt.plot([item[i] for item in secondWeightsTime], errorOverTime, label=f"neuron{i}")
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15),
          ncol=4, fancybox=True, shadow=True, fontsize='7').get_frame().set_alpha(0.5)
plt.savefig("errorOverSecondWeights.png")
plt.close()
"""

secondWeightsTimeMean = [sum(item[i]) for item in secondWeightsTime]
errorOverTimeMeanNp = np.array(errorOverTimeMEAN)

"""weired error at end of resample"""
#errorOverTimeMEAN = signal.resample(errorOverTimeMEAN, int(learningLoops*len(streetlights)*1.1))
#errorOverTimeMEAN = errorOverTimeMEAN[0:int(len(errorOverTimeMEAN)*0.9)]

np.savetxt("errorMean.txt", errorOverTimeMeanNp.squeeze(), newline="\n")
fig = plt.figure()
ax = plt.subplot()
#ax.text(0.0, 0.9, 'errorMeanOverSecondWeights')
plt.plot(secondWeightsTimeMean, errorOverTimeMeanNp.squeeze(), label=f"weightsMean", marker=11)
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15),
          ncol=4, fancybox=True, shadow=True, fontsize='7').get_frame().set_alpha(0.5)
plt.xlabel('mean weights 1->2', fontsize=10)
plt.ylabel('mean error', fontsize=10)
plt.savefig("errorMeanOverSecondWeightsMean.png")
plt.close()

fig = plt.figure()
ax = plt.subplot()
#ax.text(0.0, 0.9, 'errorMeanOverFirstWeights')
for i in range(hidden_size):
  plt.plot([item[i] for item in firstWeightsTime0], errorOverTimeMeanNp.squeeze(), label=f"input{i}_0", color=f"C{i}", marker="x", linewidth = 0)
  plt.plot([item[i] for item in firstWeightsTime1], errorOverTimeMeanNp.squeeze(), label=f"input{i}_1", color=f"C{i}", marker="x", linewidth = 0)
  plt.plot([item[i] for item in firstWeightsTime2], errorOverTimeMeanNp.squeeze(), label=f"input{i}_2", color=f"C{i}", marker="x", linewidth = 0)
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15),
          ncol=4, fancybox=True, shadow=True, fontsize='7').get_frame().set_alpha(0.5)
plt.xlabel('weights 0->1', fontsize=10)
plt.ylabel('mean error', fontsize=10)
plt.savefig("errorMeanOverFirstWeights.png")
plt.close()

np.savetxt("errorMean.txt", errorOverTimeMeanNp.squeeze(), newline="\n")
fig = plt.figure()
ax = plt.subplot()
#ax.text(0.0, 0.9, 'errorMeanOverSecondWeights')
for i in range(hidden_size):
  plt.plot([item[i] for item in secondWeightsTime], errorOverTimeMeanNp.squeeze(), label=f"neuron{i}", marker="x", linewidth = 0)
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15),
          ncol=4, fancybox=True, shadow=True, fontsize='7').get_frame().set_alpha(0.5)
plt.xlabel('weights 1->2', fontsize=10)
plt.ylabel('mean error', fontsize=10)
plt.savefig("errorMeanOverSecondWeights.png")
plt.close()

"""
fig = plt.figure()
ax = plt.subplot()
ax.text(0.0, 0.9, 'errorOverFirstWeights')
for i in range(hidden_size):
  plt.plot([item[i] for item in firstWeightsTime0], errorOverTime, label=f"input{i}_0", color=f"C{i}")
  plt.plot([item[i] for item in firstWeightsTime1], errorOverTime, label=f"input{i}_1", color=f"C{i}")
  plt.plot([item[i] for item in firstWeightsTime2], errorOverTime, label=f"input{i}_2", color=f"C{i}")
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15),
          ncol=4, fancybox=True, shadow=True, fontsize='7').get_frame().set_alpha(0.5)
plt.savefig("errorOverFirstWeights.png")
plt.close()
"""

"""
fig = plt.figure()
ax = plt.subplot()
ax.text(0.0, 0.9, 'errorOverFirstWeightsPerInput')
for n in range(hidden_size): # per input
  for i in range(hidden_size): # per neuron
    # now change i & n to have better sorting
    plt.plot([item[n] for item in firstWeightsTime0PerInput[i]], errorOverTimePerNeuron[i], label=f"neuron{n}_input{i}", color=f"C{n}")
    plt.plot([item[n] for item in firstWeightsTime1PerInput[i]], errorOverTimePerNeuron[i], label=f"neuron{n}_input{i}", color=f"C{n}")
    plt.plot([item[n] for item in firstWeightsTime2PerInput[i]], errorOverTimePerNeuron[i], label=f"neuron{n}_input{i}", color=f"C{n}")
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15),
          ncol=4, fancybox=True, shadow=True, fontsize='7').get_frame().set_alpha(0.5)
plt.savefig("errorOverFirstWeightsPerInput.png")
plt.close()

fig = plt.figure()
ax = plt.subplot()
ax.text(0.0, 0.9, 'errorOverSecondWeightsPerNeuron')
for n in range(hidden_size):
  for i in range(hidden_size):
    # changed i & n to have better sorting
    plt.plot([item[n] for item in secondWeightsTimePerInput[i]], errorOverTimePerNeuron[i], label=f"neuron{n}_input{i}", color=f"C{n}")
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15),
          ncol=4, fancybox=True, shadow=True, fontsize='7').get_frame().set_alpha(0.5)
plt.savefig(f"errorOverSecondWeightsPerNeuron.png")
plt.close()
"""