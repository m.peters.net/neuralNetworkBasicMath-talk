import numpy as np

# set seed to make random stuff reproducible
np.random.seed(1)

# activation
def relu(x):
    return (x > 0) * x # returns x if x > 0
                       # return 0 otherwise

# dericative of activation
def relu2deriv(output):
    return output>0 # returns 1 for input > 0
                    # return 0 otherwise

"""initialize"""
# input layer - 4 different inputs
streetlights = np.array( [[ 1, 0, 1 ],
                          [ 0, 1, 1 ],
                          [ 0, 0, 1 ],
                          [ 1, 1, 1 ] ] )

# output layer - 4 different outputs
walk_vs_stop = np.array([[ 1, 1, 0, 0]]).T

alpha = 0.2 # learning rate
hidden_size = 4 # number of neurons in hidden layer 

# weights - initialized randomly
weights_0_1 = 2*np.random.random((3,hidden_size)) - 1 # weights between input and hidden layer
weights_1_2 = 2*np.random.random((hidden_size,1)) - 1 # weights between hidden layer and output

# number of itterations
learningLoops = 20 * len(streetlights)

for iteration in range(learningLoops): # loops through set learning loops
   layer_2_error = 0
   for i in range(len(streetlights)): # loops through all inputs
      """predict"""
      layer_0 = streetlights[i:i+1]
      layer_1 = relu(np.dot(layer_0,weights_0_1))
      layer_2 = np.dot(layer_1,weights_1_2)

      layer_2_error += np.sum((layer_2 - walk_vs_stop[i:i+1]) ** 2)

      """compare"""
      layer_2_delta = (layer_2 - walk_vs_stop[i:i+1])
      layer_1_delta=layer_2_delta.dot(weights_1_2.T)*relu2deriv(layer_1)

      """learn"""
      weights_1_2 -= alpha * layer_1.T.dot(layer_2_delta)
      weights_0_1 -= alpha * layer_0.T.dot(layer_1_delta)

   if(iteration % 10 == 9):
      print("Error:" + str(layer_2_error)) # that should be the mean over one batch of inputs, IMHO